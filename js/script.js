$(document).ready(function() {
    
    //Animations 
    
    $('.js--wp-1').waypoint(function(direction) {
        $('.js--wp-1').addClass('animated fadeIn');
    }, {
        offset: '100%'
    });
    
    $('.js--wp-2').waypoint(function(direction) {
        $('.js--wp-2').addClass('animated fadeIn');
    }, {
        offset: '80%'
    });
    
    $('.js--wp-3').waypoint(function(direction) {
        $('.js--wp-3').addClass('animated fadeIn');
    }, {
        offset: '80%'
    });
    
    $('.js--wp-4').waypoint(function(direction) {
        $('.js--wp-4').addClass('animated fadeIn');
    }, {
        offset: '80%'
    });
    
    $('.js--wp-5').waypoint(function(direction) {
        $('.js--wp-5').addClass('animated fadeIn');
    }, {
        offset: '80%'
    });
    
    $('.js--wp-6').waypoint(function(direction) {
        $('.js--wp-6').addClass('animated fadeIn');
    }, {
        offset: '80%'
    });
    
    $('.js--wp-7').waypoint(function(direction) {
        $('.js--wp-7').addClass('animated fadeIn');
    }, {
        offset: '80%'
    });
    
    $('.js--wp-8').waypoint(function(direction) {
        $('.js--wp-8').addClass('animated fadeIn');
    }, {
        offset: '90%'
    });
    
    $('.js--wp-9').waypoint(function(direction) {
        $('.js--wp-9').addClass('animated fadeIn');
    }, {
        offset: '90%'
    });
    
    $('.js--wp-10').waypoint(function(direction) {
        $('.js--wp-10').addClass('animated fadeIn');
    }, {
        offset: '90%'
    });
    
    $('.js--wp-11').waypoint(function(direction) {
        $('.js--wp-11').addClass('animated fadeIn');
    }, {
        offset: '80%'
    });
    
    $('.js--wp-12').waypoint(function(direction) {
        $('.js--wp-12').addClass('animated fadeIn');
    }, {
        offset: '90%'
    });

    $('.js--wp-13').waypoint(function(direction) {
        $('.js--wp-13').addClass('animated fadeIn');
    }, {
        offset: '80%'
    });
    
    $('.js--wp-14').waypoint(function(direction) {
        $('.js--wp-14').addClass('animated fadeIn');
    }, {
        offset: '80%'
    });


    //sticky navigation
    
    
    $('.js--about').waypoint(function(direction) {
        if(direction == 'down') {
            $('nav').addClass('sticky');
        } else {
            $('nav').removeClass('sticky');
        }
    }, {
        offset: '100px'
    });
    
    
    /* smooth-scrolling*/

    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
          && 
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000, function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) { // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              };
            });
          }
        }
      });
   
    
    
    //E-mail Ajax Send
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			alert("Thank you!");
			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
	});
    
    /*Slider Portfolio*/

    $('.fotorama').fotorama({
        nav: 'false',
        maxwidth: '100%',
    //    fit: 'cover',
        arrows: 'always'
    });
    
    /*Image Popup For Certificate*/
    $('.certification__list').magnificPopup({
      delegate: 'a', // child items selector, by clicking on it popup will open
      type: 'image',
      // other options
        gallery: {
                enabled:true,
                preload: [1,2]
        }
    });

});




